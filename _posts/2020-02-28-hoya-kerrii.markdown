---
layout: post
title:  "Hoya Kerrii"
date:   2020-02-28
categories: jekyll update
img: /assets/img/hoya-kerrii
---

L' Hoya Kerrii est souvent mit à l'honneur lors de la saint Valentin.
Cette plante succulente en forme de coeur est irrésistible.

C'est une plante grimpante (4m. max) composée de feuilles charnues en forme de coeur (6cm.).
Généralement, la plante se présente sous la forme d'une feuille unique. Elle reste
inchangée pendant plusieurs mois, voir plusieurs années, avant d'entamer une croissance,
c'est-à-dire une transformation en plante grimpante.

### Entretiens
☀️🌤️ Attention à l'effet loupe des fenêtres

🌡️ > 15°

💧 Arrosage toutes les 2 / 3 semaines

⛏️ Terre bien drainée

### Vertu
-> Améliore la qualité de l'air

### Famille
Apocynaceae

### Origine
Asie

### Histoire
Décrite en 1911 par William Grant Craib, recueillie par Arthur Francis George Kerr en 1910.

### Aire de répartition
Chine du sud, Vietnam, Laos, Cambodge, Thaïlande, Java.

### Floraison
En été, fleurs blanches avec étamines brunes.

### Cousine
Hoya kerrii Variegata (feuille bordée d'une bande blanche)

### Noms vernaculaires
 \#luckyheart #plantedelamour #hoya #fleursdeporcelaine #fleursdecire #hoyavalentin #hoyakerrii #fleurdeporcelaine #fleurdecire #hoyavalentine #hoyavalentines #plantedelasaintvalentin

### Références
 Wikipédia et deavita.fr
