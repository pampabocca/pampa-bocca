---
layout: post
title:  "Anthurium Rouge"
date:   2019-09-19
categories: jekyll update
img: /assets/img/anthurium
---

L'anthurium rouge prends la pose dans la cuisine. Également appelé la langue de feu,
c'est une plante dépolluante (ammoniac).
Pour le moment, elle est près de la fenêtre, arrosage hebdomadaire avec les restes
de la bouilloire. De temps en temps, elle a droit à une vaporisation permettant
un dépoussiérage.

### Entretiens
🌤️

🌡️ > 15°

💧 Vaporiser les feuilles, Arroser 2 / 3 fois par semaine, Utiliser eau filtrée

⛏️ Fibre de coco

### Vertu
-> Dépolluante

### Genre
Anthurium andraeanum

### Famille  
Araceae

### Origine
Équateur et sud-ouest de la Colombie. (Naturalisée aux Antilles et île de la Réunion)
