---
layout: page
title: Plante
permalink: /plante/
---

<h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">{{page.title}}</h3>


{%- if site.posts.size > 0 -%}

  <div class="row">

    <div class="col s12">
    {%- for post in site.posts -%}

    <div class="col s12 m6 l4">


      <div class="card">
       <div class="card-image waves-effect waves-block waves-light">
         <a class="post-link" href="{{ post.url | relative_url }}">
         <picture>
           <source srcset="{{post.img | relative_url}}.webp" type="image/webp" width="305" height="230" class="responsive-img wp-post-image" alt="{{ post.title | escape }}"
             title="{{ post.title | escape }}" sizes="(max-width: 305px) 100vw, 305px">
           <img src="{{post.img | relative_url}}.png" alt="" width="305" height="230" class="responsive-img wp-post-image" alt="{{ post.title | escape }}"
             title="{{ post.title | escape }}" sizes="(max-width: 305px) 100vw, 305px">
          </picture>
            <span class="card-title home">{{ post.title | escape }}</span>
         </a>

       </div>

      </div>

    </div>

    {%- endfor -%}
    </div>
</div>
{%- endif -%}
